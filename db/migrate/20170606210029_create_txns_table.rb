require 'rein'
require_relative '../../constants.rb'

class CreateTxnsTable < ActiveRecord::Migration[5.0]

  class Txn < ActiveRecord::Base; end

  def self.up
    create_table :txns, force: true do |t|
      # sender | receiver = platform|team|member - uniquely identifies a user account

      t.string :sender, null: false
      t.string :receiver, null: false

      t.integer :amount, null: false

      t.integer :sender_new_balance, null: false
      t.integer :receiver_new_balance, null: false

      t.json :metadata
      t.string :via

      t.timestamps
    end

    add_presence_constraint :txns, :sender
    add_presence_constraint :txns, :receiver

    add_numericality_constraint :txns, :amount, greater_than_or_equal_to: 1
    add_numericality_constraint :txns, :sender_new_balance, greater_than_or_equal_to: 0
    add_numericality_constraint :txns, :receiver_new_balance, greater_than_or_equal_to: 0

    add_presence_constraint :txns, :via

    #execute "ALTER TABLE txns ADD CONSTRAINT sender_not_empty CHECK (length(regexp_replace(sender, '\s+', '')) > 0)"
    #execute "ALTER TABLE txns ADD CONSTRAINT receiver_not_empty CHECK (length(regexp_replace(receiver, '\s+', '')) > 0)"

    # sender and receiver cannot be the same
    execute "ALTER TABLE txns ADD CONSTRAINT sender_receiver_different CHECK (sender <> receiver)"
    
    # only genesis can create the first transaction and no other txn
    execute "ALTER TABLE txns ADD CONSTRAINT only_one_genesis CHECK (((id = 1) and (sender = 'genesis|genesis|genesis')) or ((id <> 1) and (sender <> 'genesis|genesis|genesis')))"

    Txn.create(
      sender: 'genesis|genesis|genesis',
      receiver: "slack|#{SLACK_TEAMS[:indium][:id]}|#{SLACK_USERS[:genesis]}",
      amount: MAX_COINS,
      sender_new_balance: 0, # No more coin creation!
      receiver_new_balance: MAX_COINS,
      metadata: {comments: 'Genesis! Txfr to genesis@indiumtalk.slack.com'},
      via: 'DB Creation script'
    )

    Txn.create(
      sender: "slack|#{SLACK_TEAMS[:indium][:id]}|#{SLACK_USERS[:genesis]}",
      receiver: "slack|#{SLACK_TEAMS[:indium][:id]}|#{SLACK_USERS[:dev]}",
      amount: (MAX_COINS * MAX_COIN_MONTHLY_RELEASE_RATE * DEV_FUND_RATIO),
      sender_new_balance: (MAX_COINS - MAX_COINS * MAX_COIN_MONTHLY_RELEASE_RATE * DEV_FUND_RATIO),
      receiver_new_balance: (MAX_COINS * MAX_COIN_MONTHLY_RELEASE_RATE * DEV_FUND_RATIO),
      metadata: {comments: 'First monthly transfer to dev@indiumtalk'},
      via: 'DB Creation script'
    )

    Txn.create(
      sender: "slack|#{SLACK_TEAMS[:indium][:id]}|#{SLACK_USERS[:dev]}",
      receiver: "slack|#{SLACK_TEAMS[:indium][:id]}|#{SLACK_USERS[:slackbotadmin]}",
      amount: 500_000,
      sender_new_balance: (MAX_COINS * MAX_COIN_MONTHLY_RELEASE_RATE * DEV_FUND_RATIO - 500_000),
      receiver_new_balance: 500_000,
      metadata: {comments: 'First payment to slackbot administrator'},
      via: 'DB Creation script'
    )

    Txn.create(
      sender: "slack|#{SLACK_TEAMS[:indium][:id]}|#{SLACK_USERS[:genesis]}",
      receiver: "slack|#{SLACK_TEAMS[:indium][:id]}|#{SLACK_USERS[:advocacy]}",
      amount: (MAX_COINS * MAX_COIN_MONTHLY_RELEASE_RATE * ADVOCACY_FUND_RATIO),
      sender_new_balance: (MAX_COINS - MAX_COINS * MAX_COIN_MONTHLY_RELEASE_RATE), # because DEV_FUND_RATIO + ADVOCACY_FUND_RATIO = 1.0
      receiver_new_balance: (MAX_COINS * MAX_COIN_MONTHLY_RELEASE_RATE * ADVOCACY_FUND_RATIO),
      metadata: {comments: 'First monthly transfer to advocacy@indiumtalk'},
      via: 'DB Creation script'
    )

    Txn.create(
      sender: "slack|#{SLACK_TEAMS[:indium][:id]}|#{SLACK_USERS[:advocacy]}",
      receiver: "slack|#{SLACK_TEAMS[:indium][:id]}|#{SLACK_USERS[:slackuserpromo]}",
      amount: 350_000,
      sender_new_balance: (MAX_COINS * MAX_COIN_MONTHLY_RELEASE_RATE * ADVOCACY_FUND_RATIO - 350_000), # 2000x100 for main, 5000x10 for printsend and 1000x100 for sawaal
      receiver_new_balance: 350_000,
      metadata: {comments: 'First batch for slack user joining reward'},
      via: 'DB Creation script'
    )

    Txn.create(
      sender: "slack|#{SLACK_TEAMS[:indium][:id]}|#{SLACK_USERS[:advocacy]}",
      receiver: "slack|#{SLACK_TEAMS[:indium][:id]}|#{SLACK_USERS[:slackadminpromo]}",
      amount: 200_000,
      sender_new_balance: (MAX_COINS * MAX_COIN_MONTHLY_RELEASE_RATE * ADVOCACY_FUND_RATIO - 350_000 - 200_000), #only sawaal is approved for admin bonus right now
      receiver_new_balance: 200_000,
      metadata: {comments: 'First batch for slack admin/introducer rewards'},
      via: 'DB Creation script'
    )
  end

  def self.down
    drop_table :txns
  end
end
