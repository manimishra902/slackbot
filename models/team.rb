require_relative '../constants'
# Represents a Slack team which has installed our bot
class Team < ActiveRecord::Base
  validates :team_id, presence: true, format: { with: SLACK_TEAM_REGEXP }
  validates :name, presence: true
  validates :active, inclusion: { in: [true, false] } # is a boolean so can't use presence because false.blank? = true

  validates :join_bonus, numericality: { only_integer: true, greater_than: -1, less_than: 100_000 }

  validates :max_user_join_bonus_count, numericality: { only_integer: true, greater_than: -1 }
  validates :actual_user_join_bonus_count, numericality: { only_integer: true, greater_than: -1 }

  validates :max_user_join_bonus_amount, numericality: { only_integer: true, greater_than: -1 }
  validates :actual_user_join_bonus_amount, numericality: { only_integer: true, greater_than: -1 }

  validates :max_admin_reward, numericality: { only_integer: true, greater_than: -1 }
  validates :actual_admin_reward, numericality: { only_integer: true, greater_than: -1 }

  validates :max_introducer_reward, numericality: { only_integer: true, greater_than: -1 }
  validates :actual_introducer_reward, numericality: { only_integer: true, greater_than: -1 }

  #after_create :reward_admin #will reward manually first

  def reward_admin
    Team.transaction do
      puts "Inside Team#reward_admin for #{self.team_id} #{self.name}"

      unless self.active
        return false, "Team #{self.id} #{self.team_id} is not active"
      end

      if self.actual_admin_reward + SLACK_OWNER_INSTALL_REWARD > self.max_admin_reward
        return false, "Max reward is #{self.max_admin_reward}. Trying to give #{SLACK_OWNER_INSTALL_REWARD} but #{self.actual_admin_reward} already given."
      end
      
      owner_uid = self.get_owner
      puts "Got owner for #{team.inspect} as #{owner_uid}"
      owner = Account.new(platform: "slack", team: self.team_id, member: owner_uid)
      Txn.pay(Account.slack_admin_promo, owner, SLACK_OWNER_INSTALL_REWARD, metadata = {comments: "Admin's reward for installing bot"}, via = "slackbot")
    end
  end

  def reward_introducer(introducer_account)
    Team.transaction do
      puts "Inside Team#reward_introducer for #{self.team_id} #{self.name} to #{introducer_account}"

      unless self.active
        return false, "Team #{self.id} #{self.team_id} is not active"
      end

      if self.actual_introducer_reward + NEW_SLACK_INTRODUCER_REWARD > self.max_introducer_reward
        return false, "Max reward is #{self.max_introducer_reward}. Trying to give #{NEW_SLACK_INTRODUCER_REWARD} but #{self.actual_introducer_reward} already given."
      end
      
      Txn.pay(Account.slack_admin_promo, introducer_account, NEW_SLACK_INTRODUCER_REWARD, metadata = {comments: "Introducer's reward for Team #{team.name}"}, via = "slackbot")
    end
  end

  def get_owner(slack_api_client = nil)
    slack_api_client = Slack::Web::Client.new(token: self.token) unless slack_api_client
    # slack_api_client.auth_test
    slack_api_client.users_list["members"].select { |v| v.is_primary_owner }.first.try(:id)
  end

  def self.reward_user_for_joining(account, tm)
    if account.platform != "slack"
      return false, "Joining reward is only available for Slack groups at the moment. Found: #{account.platform}"
    end

    team = Team.where(team_id: tm.id).where(active: true).first

    if team.nil?
      return false, "Team #{tm.name} either not registered or not active"
    end

    if team.join_bonus <= 0
      return false, "Joining bonus is disabled for your Slack team"
    end

    if team.actual_user_join_bonus_count >= team.max_user_join_bonus_count
      return false, "#{team.actual_user_join_bonus_count} users have already claimed bonus, max allowed is #{team.max_user_join_bonus_count}"
    end

    if (team.actual_user_join_bonus_amount + team.join_bonus) >= team.max_user_join_bonus_amount
      return false, "#{team.actual_user_join_bonus_amount} coins have already been claimed as joining bonus, max limit for your Slack group is #{team.max_user_join_bonus_amount}"
    end

    if Txn.txns_for(account).none? { |t| t.slack_user_bonus_txn? }
      txn, message = Txn.pay(Account.slack_user_promo, account, team.join_bonus, metadata = {comments: "joining bonus"}, via = "slackbot")
      if txn
        return true, "#{txn.sender.indium_account.slack_display_name(account.team)} sent #{txn.amount} coins to #{txn.receiver.indium_account.slack_display_name(account.team)} successfully."
      else
        return false, message
      end
    else
      txn = Txn.txns_for(account).select { |t| t.slack_user_bonus_txn? }.first
      return false, "You have already claimed joining bonus on #{txn.created_at} (Txn id: #{txn.id})"
    end
  end
end