require 'active_model'

# An account is a string of the format "platform|team|member"
# platform can be either `genesis` or `slack` currently. In future, `twitter`, `facebook`, `reddit`, `pubkey` will be added
# team is needed to handle multiple groups per platform which provides a namespace for user IDs. For eg: it would
# mean team_id in case of Slack. If not needed, it should be same as platform.
# member will be the unique ID of a user. Eventually, this is where we will support hash of a public key for anonymous accounts
# if platform = genesis, then team must be genesis and member must be genesis

class Account
  include ActiveModel::Model
  attr_accessor :platform, :team, :member

  validates :platform, presence: true, inclusion: { in: %w(genesis slack) }
  validates :member, presence: true, format: { with: /\A[a-zA-Z0-9]+\z/ }
  validates :team, presence: true, format: { with: /\A[a-zA-Z0-9]+\z/ }
  validate :validate_genesis_id
  validate :slack_name_formats

  def validate_genesis_id
    if platform == "genesis"
      errors.add(:team, "must be 'genesis' if platform is 'genesis'") unless team == "genesis"
      errors.add(:member, "must be 'genesis' if platform is 'genesis'") unless member == "genesis"
    end
  end

  def slack_name_formats
    if platform == "slack"
      errors.add(:team, "seems invalid for slack") unless team =~ SLACK_TEAM_REGEXP
      errors.add(:member, "seems invalid for slack") unless member =~ SLACK_USER_REGEXP
    end
  end

  def id
    [@platform, @team, @member].join("|")
  end

  def id=(id)
    @platform, @team, @member = *id.split("|") #splat
  end

  def to_s
    self.id.to_s
  end

  def self.from_id(id)
    a = Account.new
    a.id = id
    raise "Invalid ID: #{a.errors.inspect}" unless a.valid?
    return a
  end

  def slack_display_name(for_slack_team)
    raise "slack name requires a context" if for_slack_team.blank?
    return id unless (platform == "slack") && (for_slack_team == team) # avoid name conflict if showing in a different slack
    return "<@#{member}> (#{id})" # slack parses the first part to show as @user
  end

  # prominent accounts
  def self.genesis
    Account.new(platform: "slack", team: SLACK_TEAMS[:indium][:id], member: SLACK_USERS[:genesis])
  end

  def self.dev
    Account.new(platform: "slack", team: SLACK_TEAMS[:indium][:id], member: SLACK_USERS[:dev])
  end

  def self.advocacy
    Account.new(platform: "slack", team: SLACK_TEAMS[:indium][:id], member: SLACK_USERS[:advocacy])
  end

  def self.slack_admin_promo
    Account.new(platform: "slack", team: SLACK_TEAMS[:indium][:id], member: SLACK_USERS[:slackadminpromo])
  end

  def self.slack_user_promo
    Account.new(platform: "slack", team: SLACK_TEAMS[:indium][:id], member: SLACK_USERS[:slackuserpromo])
  end

  def self.slackbot_admin
    Account.new(platform: "slack", team: SLACK_TEAMS[:indium][:id], member: SLACK_USERS[:slackbotadmin])
  end

  def self.founder
    Account.new(platform: "slack", team: SLACK_TEAMS[:indium][:id], member: SLACK_USERS[:nilesh])
  end
end