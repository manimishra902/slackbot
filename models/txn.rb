class Txn < ActiveRecord::Base
  validates :amount, numericality: { only_integer: true, greater_than: 0 }
  validates :sender_new_balance, numericality: { only_integer: true, greater_than: -1 }
  validates :receiver_new_balance, numericality: { only_integer: true, greater_than: -1 }

  validates :sender, presence: true, format: { with: /\A[a-zA-Z0-9]+|[a-zA-Z0-9]+|[a-zA-Z0-9]+\z/}
  validates :receiver, presence: true, format: { with: /\A[a-zA-Z0-9]+|[a-zA-Z0-9]+|[a-zA-Z0-9]+\z/}
  validate :sender_receiver_are_different

  validates :via, presence: true

  scope :today,  -> { where("created_at BETWEEN ? and ?", Time.now.beginning_of_day, Time.now.end_of_day) }

  def self.txns_for(account)
    raise "no account passed in txns_for" if account.nil?
    raise "invalid account in txns_for: #{account.errors.inspect}" if account.invalid?
    Txn.where("sender = ?", account.id)
      .or(Txn.where("receiver = ?", account.id)).order(id: :desc)
  end

  def self.balance_for(account)
    raise "no account passed in balance_for" if account.nil?
    raise "invalid account in balance_for: #{account.errors.inspect}" if account.invalid?
    # TODO: Currently computing balance by going over all the txns. Need to optimize.
    self.txns_for(account).inject(0) { |sum,txn|
      if txn.sender == account.id
        sum - txn.amount
      else
        sum + txn.amount
      end
    }
  end

  def self.pay(sender, receiver, amount, metadata, via)
    Txn.transaction do
      if sender.invalid?
        return false, "Sender #{sender} is invalid: #{sender.errors.inspect}"
      end

      if receiver.invalid?
        return false, "Receiver #{receiver} is invalid: #{receiver.errors.inspect}"
      end

      if sender.id == receiver.id
        return false, "Sender and receiver can't be the same! #{sender}"
      end

      unless amount.is_a?(Integer) && amount > 0
        return false, "Amount must be a positive integer"
      end

      sender_balance = self.balance_for(sender)
      receiver_balance = self.balance_for(receiver)
      if sender_balance < amount
        return false, "Sender #{sender}'s balance is too low: #{sender_balance} for amount: #{amount}"
      end

      txn = Txn.new(
        sender: sender.id,
        receiver: receiver.id,
        amount: amount,
        sender_new_balance: (sender_balance - amount),
        receiver_new_balance: (receiver_balance + amount),
        metadata: metadata,
        via: via
      )
      if txn.save
        # TODO: create reward transaction
        return txn, "#{sender} sent #{amount} coins to #{receiver} successfully."
      else
        return false, "Transaction failed: #{txn.errors.inspect}"
      end
    end #transaction
  end

  def slack_user_bonus_txn?
    sender == "slack|#{SLACK_TEAMS[:indium][:id]}|#{SLACK_USERS[:slackuserpromo]}"
  end

  private
  def sender_receiver_are_different
    errors.add(:sender, "must be different from receiver") if self.sender == self.receiver
  end
end