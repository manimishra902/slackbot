class Togglegif < SlackRubyBot::Commands::Base
  def self.call(client, data, _match)
    logger.info "Togglegif: #{client.owner}, user=#{data.user}, message=#{data.text}, match=#{_match.inspect}"

    platform = "slack"
    team_id = client.team.id
    team_name = client.team.name
    user_id = data.user
    account = Account.new(platform: platform, team: team_id, member: user_id)

    if account.invalid? || (account.id != Account.slackbot_admin.id)
      client.say(channel: data.channel, text: "Only admin can toggle GIFs at global level".with_env)
      return
    end

    # Currently, there is no way to toggle GIFs team-wise. See https://github.com/slack-ruby/slack-ruby-bot/issues/148
    SlackRubyBot.configure do |config|
      if config.send_gifs
        config.send_gifs = false
        client.say(channel: data.channel, text: "GIFs are now disabled!".with_env)
      else
        config.send_gifs = true
        client.say(channel: data.channel, text: "GIFs are now enabled!".with_env)
      end
    end
  end
end
