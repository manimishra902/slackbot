class Eval < SlackRubyBot::Commands::Base
  def self.call(client, data, _match)
    logger.info "EVAL: #{client.owner}, user=#{data.user}, message=#{data.text} match=#{_match.inspect}"
    platform = "slack"
    team_id = client.team.id
    team_name = client.team.name
    user_id = data.user
    account = Account.new(platform: platform, team: team_id, member: user_id)

    if account.invalid? || (account.id != Account.slackbot_admin.id)
      client.say(channel: data.channel, text: "Only admin can do remote-debugging.")
      return
    end

    if _match['expression'].start_with?((ENV['EVAL_SECRET'] || "secret") + " ")
      result = eval(_match['expression'].gsub((ENV['EVAL_SECRET'] || "secret") + " ", ""))
      client.say(channel: data.channel, text: result.to_s.with_env)
    else
      client.say(channel: data.channel, text: "Eval not allowed. Use secret?".with_env)
    end
    #client.say(channel: data.channel, gif: 'help')
  end
end