class Stats < SlackRubyBot::Commands::Base
  def self.call(client, data, _match)
    logger.info "STATS: #{client.owner}, user=#{data.user}, message=#{data.text}, match=#{_match.inspect}"

    platform = "slack"
    team_id = client.team.id
    team_name = client.team.name
    user_id = data.user

    msg = "Active Teams: #{Team.active.count}, Transactions: #{Txn.count}"
    msg += "\n\nProminent Account balances:"
    msg += "\n*genesis*: #{Txn.balance_for(Account.genesis)}"
    msg += "\n*dev*: #{Txn.balance_for(Account.dev)}"
    msg += "\n*advocacy*: #{Txn.balance_for(Account.advocacy)}"
    msg += "\n*slackadminpromo* (pool account for rewarding admins for installing bot): #{Txn.balance_for(Account.slack_admin_promo)}"
    msg += "\n*slackuserpromo* (pool account for rewarding early slackbot users): #{Txn.balance_for(Account.slack_user_promo)}"
    msg += "\n*slackbotadmin* (administrator of slackbot): #{Txn.balance_for(Account.slackbot_admin)}"
    msg += "\n*nilesh* (project founder): #{Txn.balance_for(Account.founder)}"
    msg += "\n\nList of active teams using the bot: #{Team.active.all.collect(&:name).join(', ')}"
    client.say(channel: data.channel, text: msg.with_env)

  end
end
